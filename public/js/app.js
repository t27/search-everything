var app = angular.module("app", []);
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  });

app.controller("ctrl", function($scope, $http, $window) {
  var google_url, dropbox_url;
  var openedWindow;

  var checkGoogleAuth = function() {
    $http.get("/auth/google/").then(function(response) {
      console.log(response.data);
      if(response.data.auth == true) {
        $scope.google_auth = true;
      } else {
        $scope.google_auth = false;
        console.log("AuthUrl=",response.data);
        google_url = response.data.url;
      }
    });
  }

  var checkDropboxAuth = function() {
    $http.get("/auth/dropbox/").then(function(response) {
      console.log(response.data);
      if(response.data.auth == true) {
        $scope.dropbox_auth = true;
      } else {
        $scope.dropbox_auth = false;
        console.log("AuthUrl=",response.data);
        dropbox_url = response.data.url;
      }
    });
  }

  checkGoogleAuth();
  checkDropboxAuth();


  $scope.google_login = function() {
    if(!openedWindow) {
      openedWindow = $window.open(google_url, "Please sign in with Google", "width=500px,height:700px");
    }
  };

  $scope.dropbox_login = function() {
    if(!openedWindow) {
      openedWindow = $window.open(dropbox_url, "Please sign in with Dropbox", "width=500px,height:700px");
    }
  }
  $scope.search = function() {
      var query = $scope.search_q;
      console.log("Searching for "+ query);
      $http.get("/search",{params:{q: query, u:"tarang27"}}).then(function(response){
        console.log("results");
        console.log(response.data);
      })
  }
  window.onmessage = function(e) {
    openedWindow.close();
    console.log("Callback URL=",e.data);
    var urlWithCode = e.data;
    var typeIdx = urlWithCode.indexOf("type=") + 5;
    var type = urlWithCode.substr(typeIdx,urlWithCode.substr(typeIdx).search(/[&#]+/));
    var idx, code;
    if(type == "google") {
      idx = urlWithCode.lastIndexOf("code=");
      code = urlWithCode.substring(idx + 5).replace("#","");
    } else if(type == "dropbox") {
      idx = urlWithCode.lastIndexOf("access_token=")+13;
      code = urlWithCode.substring(idx, urlWithCode.indexOf("&", idx));
    }

    console.log(type);
    console.log(code);
    // var username="tarang27";
    // $http.get("/auth/"+type+"/cb?"+"&u="+username+"&code=" + code).then(function(response) {
    $http.get("/auth/"+type+"/cb?"+"code=" + code).then(function(response) {
      console.log(response.data);
      openedWindow = null;
      checkGoogleAuth();
      checkDropboxAuth();
    });
  };
});
