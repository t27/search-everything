var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var UserSchema = mongoose.Schema({
	username: {
		type: String,
		index:true
	},
	password: {
		type: String
	},
	email: {
		type: String
	},
	name: {
		type: String
	},
  tokens: mongoose.Schema.Types.Mixed // Json object with token type as key and token object as data
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = function(newUser, callback){
	bcrypt.genSalt(10, function(err, salt) {
	    bcrypt.hash(newUser.password, salt, function(err, hash) {
	        newUser.password = hash;
	        newUser.save(callback);
	    });
	});
}

module.exports.getUserByUsername = function(username, callback){
	var query = {username: username};
	User.findOne(query, callback);
}

module.exports.getUserById = function(id, callback){
	User.findById(id, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
	bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    	if(err) throw err;
    	callback(null, isMatch);
	});
}

module.exports.saveToken = function(username, type, tokenData) {
  User.findOne({ "username":username }, function(err, user) {
    console.log("Name=", username);
    console.log("Got User=", user);
    if(err) {
      throw err;
    }
    if(!(user.tokens)) {
      user.tokens = {};
    }
    user.tokens[type] = tokenData;
    user.markModified('tokens');
    console.log("Type=",type,"\nToken=", tokenData, "\nUser=", user);
    user.save(function (err,user) {
      if (err) {
        console.log(err);
      } else {
        console.log('meow');
        console.log("Saved = ", user);
      }
    });
  })
}
