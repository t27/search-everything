
exports.Google = { // Enter Google App credentials here, remember to enable Drive API
  project_id: "search-everything",
  client_id: "",
  client_secret: "",
  cb: "http://localhost:3000/cb?type=google"
}

exports.Dropbox = { // Enter Dropbox App Creds here
  client_id: "",
  client_secret: "",
  cb: "http://localhost:3000/cb?type=dropbox"
}
