var express = require('express');
var Dropbox = require('dropbox');
var router = express.Router();
var config = require('../config');
var User = require('../models/user');


var scopes = ['https://www.googleapis.com/auth/drive.metadata.readonly'];

var dbx = new Dropbox({ clientId: config.Dropbox.client_id });

/**
 * [checkAuth description]
 * @param  {[type]}   username [description]
 * @param  {Function} callback Callback(err, authClient)
 * @return {[type]}            Returns auth client if successful, else returns true error
 */
module.exports.checkAuth = function (username,callback) {
  User.findOne({'username':username}, function(err, user) {
    if(err) throw err;
    if(user && user.tokens && user.tokens.dropbox) {
      // Dropbox user is authenticated
      var dbx = new Dropbox(user.tokens.dropbox);
      console.log("Auth1=", dbx, "Tokens =", user.tokens.dropbox);
      callback(err, dbx);
    } else {
      callback(true);
    }
  });
};

var getAuthUrl = function () {
  var dbx = new Dropbox({ clientId: config.Dropbox.client_id });
  return dbx.getAuthenticationUrl(config.Dropbox.cb);
};

var saveTokens = function(err, tokens, username) {
  if (err) {
    console.log(err);
    callback(err);
    return;
  }
  console.log("allright!!!!");

  console.log(err);
  console.log(tokens);
  User.saveToken(username, "dropbox", tokens)
}

var authCallback = function (req, res) {
  var username = req.user.username;
  var code = req.query.code;
  saveTokens(false, {"accessToken": code}, username);
};

module.exports.search = function(query, auth, callback) {
  console.log("auth= ",auth)
  auth.filesSearch({
    "path": "",
    "query": query,
    "start": 0,
    "max_results": 100,
    "mode": "filename"
  }).then(function(response) {
    console.log(response);
    callback(response);
  })
}

router.get('/', function (req, res) {
  // Check Google auth
  var user = req.user.username;
  console.log("Checking Dropbox Auth for ",user);
  module.exports.checkAuth(user,function(err, isAuth) {
    if(err || !isAuth) {
      // Not authenticated
      res.json({
        auth:false,
        url:getAuthUrl()
      });
    } else {
      res.json({
        auth:true
      });
    }
  });
});

router.get('/cb', authCallback);

module.exports.router = router;
