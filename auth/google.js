var express = require('express');
var google = require('googleapis');

var router = express.Router();
var config = require('../config');
var User = require('../models/user');

var OAuth2 = google.auth.OAuth2;

var scopes = ['https://www.googleapis.com/auth/drive.metadata.readonly'];
var oauth2Client = new OAuth2(
  config.Google.client_id,
  config.Google.client_secret,
  config.Google.cb
);

/**
 * [checkAuth description]
 * @param  {[type]}   username [description]
 * @param  {Function} callback Callback(err, authClient)
 * @return {[type]}            Returns auth client if successful, else returns true error
 */
module.exports.checkAuth = function (username,callback) {
  User.findOne({'username':username}, function(err, user) {
    if(err) throw err;
    if(user && user.tokens && user.tokens.google) {
      console.log("Tokens = ", user.tokens.google);
      oauth2Client.credentials = user.tokens.google;
      if(user.tokens["google"].expiry_date < Date.now()) {
        console.log("expiry date is less than now"); // IE Token has expired, generate new ones
        oauth2Client.refreshAccessToken(function(err, tokens) {
          if(err) {
            throw err;
          } else {
            oauth2Client.credentials = tokens;
            saveTokens(err, tokens, user.username);
            callback(null,oauth2Client);
          }
        });
      } else {
        callback(null,oauth2Client);
      }
    } else {
      callback(true);
    }
  });
};

var getAuthUrl = function (callbackUrl) {
  return oauth2Client.generateAuthUrl({
    access_type: 'offline', // 'online' (default) or 'offline' (gets refresh_token)
    scope: scopes, // If you only need one scope you can pass it as string
    prompt: 'consent'
  });
};

var saveTokens = function(err, tokens, username) {
  if (err) {
    console.log(err);
    callback(err);
    return;
  }
  console.log("allright!!!!");

  console.log(err);
  console.log(tokens);
  oauth2Client.setCredentials(tokens);

  User.saveToken(username, "google", tokens)
  // callback(null, tokens);
  // Query User by username
  // Save token to user object
  // oauth2Client.setCredentials(tokens);
}

var authCallback = function (req, res) {
  var username = req.user.username;
  var code = req.query.code;
  console.log("User=",username,"\nCode=",code);
  oauth2Client.getToken(code, function(err, tokens) {
    if(err) {
      res.status(400).send(err);
    } else {
      saveTokens(err, tokens, username);
      res.status(200).send("Done");
    }
  });
};

module.exports.search = function(query, auth, callback) {
  console.log("auth= ",auth)
  var service = google.drive('v3');
  service.files.list({
    auth: oauth2Client,
    pageSize: 10,
    q:"name contains '"+query+"'",
    fields: "nextPageToken, files(id, name)"
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var files = response.files;
    if (files.length == 0) {
      console.log('No files found.');
    } else {
      console.log('Files:');
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        console.log('%s (%s)', file.name, file.id);
      }
      callback(files);
    }
  });
}

router.get('/', function (req, res) {
  // Check Google auth
  var user = req.user.username;
  console.log("Checking Google Drive Auth for ",user);
  module.exports.checkAuth(user,function(err, isAuth) {
    if(err || !isAuth) {
      // Not authenticated
      res.json({
        auth:false,
        url:getAuthUrl(config.Google.cb)
      });
    } else {
      res.json({
        auth:true
      });
    }
  })
});

router.get('/cb', authCallback);

module.exports.router = router;
