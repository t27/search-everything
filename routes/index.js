var express = require('express');
var router = express.Router();

var google_auth = require('../auth/google');
var dropbox_auth = require('../auth/dropbox');

// Get Homepage
router.get('/', ensureAuthenticated, function(req, res){
	res.render('index', {user:req.user});
  // Check the current user and see what service(drive, dropbox etc)  are authenticated, show a view which reflects this
});

router.use('/auth/google', ensureAuthenticated, google_auth.router);
router.use('/auth/dropbox', ensureAuthenticated, dropbox_auth.router);

router.get('/search', ensureAuthenticated, function(req,res){
  var query = req.query.q;
  var user = req.user;
  google_auth.checkAuth(user.username, function(err, authClient) {
    if(!authClient) {
      res.send("error");
    } else {
      google_auth.search(query, authClient, function(results){
        dropbox_auth.checkAuth(user.username, function(err, authClient){
          if(!authClient){
            res.send(results);
          } else {
            dropbox_auth.search(query, authClient, function(dbresults){
              var finalResult = {};
              finalResult["google"] = results;
              finalResult["dropbox"] = dbresults;
              res.json(finalResult); // REPLACE WITH HANDLEBARS IMPLEMENTATION
              //merge results and dbresults and res.json(results);
            })
          }
        })

      });
    }
  });
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()) {
		return next();
	} else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}

module.exports = router;
